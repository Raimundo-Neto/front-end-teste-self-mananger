// Controller
app.controller('dentistaController', function ($scope, dentistService, $timeout) {
	$scope.index = function () {

		$scope.modalCreate = false;

		dentistService.index().then(function (data) {

			$scope.dentists = data.data;

			console.log(data);

		}).catch(function (error) {
			console.log(error);
		});
	}

	$scope.editar = function (data = undefined) {

		if (data == undefined) {
			$scope.closeModalCreate();
		} else {
			dentistService.edit(data).then(function (res) {
				$scope.dentista = res.data[0];
				console.log($scope.dentista, 'no edit');
				$scope.closeModalCreate();
			})


		}


	}
	$scope.pushPhone = function () {

		if ($scope.dentista.personal_data.contact.phones == undefined) {
	
			$scope.dentista.personal_data.contact.phones = [];

		}
		$scope.dentista.personal_data.contact.phones.push($scope.nr_phone);

		console.log($scope.dentista);

	}
	$scope.popPhone = function ($id) {

		$scope.dentista.personal_data.contact.phones.pop($id);

	}

	$scope.closeModalCreate = function () {
		console.log('modalants',$scope.modalCreate);
		if ($scope.modalCreate == true) {

			$scope.modalCreate = false;
		} else {
			$scope.modalCreate = true;

		}

		console.log('modaldepois',$scope.modalCreate);

	}

	$scope.salvar = function (id = undefined) {
		console.log($scope.dentista, id, 'tou no salvar');
		if (id != undefined) {

			dentistService.update($scope.dentista, id).then(function (res) {

				$scope.index();

				$scope.response(res);

			}).catch(function (error) {
                console.log(error);
                $scope.response(error);
			});
		} else {
			console.log('tou no store');
			dentistService.store($scope.dentista).then(function (res) {
				$scope.index();
				$scope.response(res);

			}).catch(function (error) {
                console.log(error);
                $scope.response(error);
			});
		}
	}

	$scope.response = function (res) {

		$scope.success = res.data.success;
		$scope.errors = res.data.errors;

		if (res.data.success != undefined) {
			console.log('entrou no');
			$scope.closeModalCreate();

		}


		$timeout(function () {
			$scope.success = undefined;
			$scope.errors = undefined;
		}, 10000);



	}


	$scope.destroy = function (id) {
		console.log(id, 'deletar');
		if (confirm("Tem certeza que deseja excluir?")) {
			dentistService.destroy(id).then(function (res) {
				console.log(res);
				$scope.index();
			}).catch(function (error) {
				console.log(error);
			});
		}
	}


});