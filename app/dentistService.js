// Service
app.factory('dentistService', function ($http) {
	return {
		index: function () {
			return $http.get(server + '/api/v1/dentists');
		},
		edit: function (id) {
			return $http.get(server + '/api/v1/dentists/' + id + '/edit');
		},
		store: function (data) {
			return $http.post(server + '/api/v1/dentists', data);
		},
		update: function (data, id) {

			return $http.put(server + '/api/v1/dentists/' + id, data);
		},
		destroy: function (id) {
			return $http.delete(server + '/api/v1/dentists/' + id)
		}
	}
});
