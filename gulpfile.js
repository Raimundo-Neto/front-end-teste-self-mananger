var gulp     = require('gulp');
var plumber  = require('gulp-plumber');
var uglify   = require('gulp-uglify-es').default;
var concat   = require('gulp-concat');
var rename   = require('gulp-rename');

var sass = require('gulp-sass');
 
sass.compiler = require('node-sass');
 

 
// Dist Path
var js_dist  = "./dist/js/";
var js_app_dist_name = "app.js";


var css_dist  = "./dist/css/";

 
// Minify e Concat Scripts
gulp.task('app', function() {
	return gulp.src(['./node_modules/angular/angular.min.js','./app/app.js', './app/dentistService.js','./app/dentistController.js','./node_modules/ng-mask/dist/ngMask.min.js'])
        .pipe(plumber())
        .pipe(uglify({ mangle: false }))
		.pipe(concat(js_app_dist_name))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(js_dist));
});

//  Build Styles
gulp.task('style', function () {
    return gulp.src('./app/css/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest(css_dist));
  });

gulp.task('style-bootstrap', function () {
    return gulp.src('./node_modules/bootstrap/dist/css/bootstrap.min.css')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest(css_dist));
  });

  gulp.task('generate-style',gulp.parallel('style','style-bootstrap'));


// Watch Modificações

  gulp.task('watch', function() {
	gulp.watch(['./app/css/*.scss'], gulp.series('style'));
	gulp.watch(['./app/*.js'], gulp.series('app'));
});


gulp.task('default', gulp.parallel('app','generate-style'));