# Front-End-Teste-SelfMananger



Simples crud desenvolvido com:

  - AngularJS(1.7.9)


# Instalação

  - Clone a aplicação:
  >> git clone https://gitlab.com/Raimundo-Neto/front-end-teste-self-mananger.git
  
  - Navegue até a raiz da aplicação e instale as dependencias:
   >> npm install

- vá até app/app.js e configure a variavel server com o host da aplicação


# Execução

Abra um terminal na raiz da aplicação e execute o gulp e depois abra o index.html no navegador
>> gulp



